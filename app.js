$(window).on("load",function() {
  $(window).scroll(function() {
    var windowBottom = $(this).scrollTop() + $(this).innerHeight();
    $(".fade").each(function() {
      /* Check the location of each desired element */
      var objectBottom = $(this).offset().top + 200;
      
      /* If the element is completely within bounds of the window, fade it in */
      if (objectBottom < windowBottom) { //object comes into view (scrolling down)
        if ($(this).css("opacity")==0) {$(this).fadeTo(500,1);}
      } 
      // else { //object goes out of view (scrolling up)
      //   if ($(this).css("opacity")==1) {$(this).fadeTo(500,0);}
      // }
    });
  }).scroll(); //invoke scroll-handler on page-load
});

const frontimg = document.querySelector(".frontimg");
const slider = document.querySelector(".slider");
const headline = document.querySelector(".headline");

const t1=new TimelineMax();

t1.fromTo(frontimg,1,{height:'0%'},{height:'80%',ease: Power2.easeInOut})
  .fromTo(frontimg,1.2,{width:'100%'},{width:'93%',ease: Power2.easeInOut})
  .fromTo(slider,1.2,{x:"-100%"},{x:"0%",ease: Power2.easeInOut},"-=1.2")
  .fromTo(headline,1.2,{opacity:'0%'},{opacity:'100%',ease: Power2.easeInOut},"-=1.2")

// card loading effect
const card1 = document.querySelector(".card1");
const cardTitle = document.querySelectorAll(".card-title");


const t2=new TimelineMax();

t2.fromTo(card1,2,{opacity:'0%'},{opacity:'100%',ease: Power2.easeInOut})
  .fromTo(cardTitle,1,{transform: "rotate(0deg) skew(0deg)"},{transform: "rotate(-5deg) skew(-5deg)",ease: Power2.easeInOut})
  
  